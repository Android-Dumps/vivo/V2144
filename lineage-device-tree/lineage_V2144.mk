#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from V2144 device
$(call inherit-product, device/vivo/V2144/device.mk)

PRODUCT_DEVICE := V2144
PRODUCT_NAME := lineage_V2144
PRODUCT_BRAND := vivo
PRODUCT_MODEL := V2144
PRODUCT_MANUFACTURER := vivo

PRODUCT_GMS_CLIENTID_BASE := android-vivo-rev1

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_mssi_64_cn_armv82-user 12 SP1A.210812.003 compiler05111007 release-keys"

BUILD_FINGERPRINT := vivo/V2144/V2144:12/SP1A.210812.003/compiler05111008:user/release-keys
