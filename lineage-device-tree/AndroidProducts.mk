#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_V2144.mk

COMMON_LUNCH_CHOICES := \
    lineage_V2144-user \
    lineage_V2144-userdebug \
    lineage_V2144-eng
